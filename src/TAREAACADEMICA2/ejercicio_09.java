package TAREAACADEMICA2;

import java.util.Scanner;

public class ejercicio_09 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("\nIngrese las dimensiones del paralelepípedo (a, b, c):");
        double a = scanner.nextDouble();
        double b = scanner.nextDouble();
        double c = scanner.nextDouble();


        double volumenParalelepipedo = calcularVolumenParalelepipedo(a, b, c);


        System.out.println("\nIngrese las dimensiones del cono (radio, altura):");
        double radio = scanner.nextDouble();
        double altura = scanner.nextDouble();

        double volumenCono = calcularVolumenCono(radio, altura);


        if (volumenParalelepipedo >= 500 && volumenParalelepipedo <= 1200 &&
                volumenCono >= 500 && volumenCono <= 1200) {
            System.out.println("\nVALIDO");
        } else {
            System.out.println("\nNO VALIDO");
        }

    }


    public static double calcularVolumenParalelepipedo(double a, double b, double c) {
        double volumenparalelepipedo;
       volumenparalelepipedo= a * b * c;
       return volumenparalelepipedo;
    }


    public static double calcularVolumenCono(double radio, double altura) {
        double volumencono;
        volumencono=(Math.PI * radio*radio * altura) / 3;
        return volumencono;
    }
}

