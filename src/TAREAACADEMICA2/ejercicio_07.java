package TAREAACADEMICA2;

import java.util.Scanner;

public class ejercicio_07 {

    public static double calcularVentaConComision(double cantidadVendida) {
        double comision;


        if (cantidadVendida >= 2000 && cantidadVendida <= 10000) {

            comision = cantidadVendida * 0.07;
        } else if (cantidadVendida > 10000 && cantidadVendida <= 20000) {

            comision = cantidadVendida * 0.10;
        } else if (cantidadVendida > 20000) {

            comision = cantidadVendida * 0.15;
        } else {

            comision = 0;
        }


        double ventaConComision = cantidadVendida + comision;


        return ventaConComision;
    }

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);


        System.out.print("\nIngrese la cantidad vendida por el ejecutivo de ventas: ");
        double cantidadVendida = scanner.nextDouble();


        double ventaConComision = calcularVentaConComision(cantidadVendida);


        System.out.println("\nLa venta sumada a la comisión es: " + ventaConComision);

    }
}