package TAREAACADEMICA2;

import java.util.Scanner;

public class ejercicio_10 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);


        System.out.println("\nIngrese el tipo de servicio (Distrital, Interprovincial o Interdepartamental):");
        String tipoServicio = scanner.nextLine();


        System.out.println("\nIngrese la distancia del viaje en kilómetros:");
        double distancia = scanner.nextDouble();


        System.out.println("\nIngrese el número de pasajeros:");
        int numPasajeros = scanner.nextInt();


        double costodeViaje = 0.0;
        if (tipoServicio.equalsIgnoreCase("Distrital")) {
            if (numPasajeros >= 1 && numPasajeros <= 3) {
                costodeViaje = distancia / 10 * 0.50;
            } else if (numPasajeros >= 4 && numPasajeros <= 5) {
                costodeViaje = distancia / 10 * 0.70;
            }
            else
                System.out.println("\nNumero máximo de pasajeros '5' ");

        } else if (tipoServicio.equalsIgnoreCase("Interprovincial")) {
            if (numPasajeros >= 1 && numPasajeros <= 3) {
                costodeViaje = distancia / 10 * 0.80;
            } else if (numPasajeros >= 4 && numPasajeros <= 5) {
                costodeViaje = distancia / 10 * 0.90;
            }
            System.out.println("\nNumero máximo de pasajeros '5' ");

        } else if (tipoServicio.equalsIgnoreCase("Interdepartamental")) {
            if (numPasajeros >= 1 && numPasajeros <= 3) {
                costodeViaje = distancia / 10 * 1.25;
            } else if (numPasajeros >= 4 && numPasajeros <= 5) {
                costodeViaje = distancia / 10 * 1.50;
            }
            System.out.println("\nNumero máximo de pasajeros '5' ");
        }
        System.out.println("\n Monto a cobrar por viaje es: S/ " + costodeViaje);

    }
}