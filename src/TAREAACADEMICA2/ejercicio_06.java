package TAREAACADEMICA2;

import java.util.Scanner;

public class ejercicio_06 {
    public static void main(String[] args) {
        double ingreso_mensual ;
        double descuento_aportacion,nuevo_ingreso;
        String tipo_aporte;
        Scanner entrada = new Scanner(System.in);

        System.out.print("\nIngrese su ingreso bruto: ");
        ingreso_mensual = entrada.nextDouble();

        System.out.print("\nIngrese su tipo de aporte 'ONP' o 'AFP': ");
        tipo_aporte = entrada.next();

        if (tipo_aporte.equals("ONP")) {
            descuento_aportacion=ingreso_mensual * 0.12;
            nuevo_ingreso=ingreso_mensual-descuento_aportacion;
            System.out.printf("\nSu descuento por aportacion es %.2f "+"y Su ingreso líquido es %.2f ",descuento_aportacion,nuevo_ingreso);

        }
        else if (tipo_aporte.equals("AFP")) {
            descuento_aportacion=ingreso_mensual * 0.17;
            nuevo_ingreso=ingreso_mensual-descuento_aportacion;
            System.out.printf("\nSu descuento por aportacion es %.2f "+"y Su ingreso líquido es %.2f ",descuento_aportacion,nuevo_ingreso);

        }
        else
            System.out.println("\nIngrese tipo de aporte válido");


    }
}
