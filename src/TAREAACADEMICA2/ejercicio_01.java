package TAREAACADEMICA2;

import java.util.Scanner;

public class ejercicio_01 {
    public static void main(String[] args) {

        int numero1, par;

        Scanner sc = new Scanner(System.in);

        System.out.println("\nIngrese numero: ");
        numero1 = sc.nextInt();

        par = numero1 %2 ;

        if (par ==0){
            System.out.println("\nEl numero es par ");
            if (numero1 > 0)
                System.out.println("\nEl numero es mayor que 0");
            else
                System.out.println("\nEl numero es menor que 0");
        }
        else if (par ==1) {
            System.out.println("\nEl numero es impar ");
            if (numero1 < 0)
                System.out.println("\nEl numero es menor que 0");
            else
                System.out.println("\nEl numero es mayor que 0");
        }

    }
}
