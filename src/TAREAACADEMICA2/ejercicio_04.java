package TAREAACADEMICA2;

import java.util.Scanner;

public class ejercicio_04 {
    public static void main(String[] args) {
        double alto,ancho;
        double galones_por_metro=0.25;

        Scanner entrada = new Scanner(System.in);
        System.out.println("\nIngrese el alto de la pared en metros : ");
        alto = entrada.nextDouble();

        System.out.println("\nIngrese el ancho de la pared en metros : ");
        ancho = entrada.nextDouble();

        calcular_metros(alto,ancho,galones_por_metro);

    }

    private static void calcular_metros(double alto, double ancho, double galones_por_metro) {
        double area_pared;
        area_pared=alto*ancho;
        int galones_necesario = (int) (area_pared * 0.25);
        if (galones_necesario>10) {
            System.out.print("\nCantidad de disolventes necesarios " + galones_necesario + " marca CPP.");
        }
        else
            System.out.print("\nCantidad de disolventes necesarios " + galones_necesario + " marca PATO LATEX.");

    }
}
