package Pruebas01;

public class trabajo01 {

    public static void main(String[] args) {
        //declaracion de variables
        int numero1;
        int numero2;
        int resultado;

        //inicializacion de variables
        numero1 = 5;
        numero2 = 10;
        resultado = 0;

        //operaciones
        resultado = numero1 + numero2;

        //mostrar resultados
        System.out.println("\nEl resultado de la suma es: " + resultado);
    }
}
