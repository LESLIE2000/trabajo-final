package taller_ejercicios;

import java.util.Scanner;

public class are_triangulo_rectangulo {
    public static void main(String[] args) {

        int a, b, c;
        int area;
        Scanner entrada = new Scanner(System.in);

        System.out.println("\nIngrese el valor de a :  ");
        a = entrada.nextInt();

        System.out.println("\nIngrese el valor de b : ");
        b = entrada.nextInt();

        System.out.println("\nIngrese el valor de c : ");
        c = entrada.nextInt();

        area = b*c+(a-c)*b/2;
        System.out.println("\nEl area calculada es: " + area);

    }

}
