package clase_03;

import java.util.Scanner;

public class ejercicio_06 {

    public static void main(String[] args) {

        //01. Declaración de variables
        Scanner sc = new Scanner(System.in);
        String sensor1, sensor2, sensor3;
        String estado_del_Sensor1, estado_del_Sensor_2, estadoSensor3;

        //02.Captura de Datos
        System.out.println("Ingrese el sensor 1: ");
        sensor1 = sc.nextLine();
        System.out.println("Ingrese el estado del sensor 1: ");
        estado_del_Sensor1 = sc.nextLine();

        System.out.println("Ingrese el sensor 2: ");
        sensor2 = sc.nextLine();
        System.out.println("Ingrese el estado del sensor 2: ");
        estado_del_Sensor_2 = sc.nextLine();

        System.out.println("Ingrese el sensor 3: ");
        sensor3 = sc.nextLine();
        System.out.println("Ingrese el estado del sensor 3: ");
        estadoSensor3 = sc.nextLine();

        //03.Calculos
        int puntaje1 = calcularPuntajeXSensor(sensor1,estado_del_Sensor1);
        System.out.printf("El puntaje de severidad para el sensor 1 es: %d \n", puntaje1);

        int puntaje2 = calcularPuntajeXSensor(sensor2,estado_del_Sensor_2);
        System.out.printf("El puntaje de severidad para el sensor 2 es: %d \n", puntaje2);

        int puntaje3 = calcularPuntajeXSensor(sensor3,estadoSensor3);
        System.out.printf("El puntaje de severidad para el sensor 3 es: %d \n", puntaje3);

        //Para probar método calcularPuntajeTotal
        int puntajeTotal = calcularPuntajeTotal(sensor1,estado_del_Sensor1,sensor2,estado_del_Sensor_2,sensor3,estadoSensor3);
        System.out.printf("El puntaje total es: %d \n", puntajeTotal);

        //Para probar método obtenerPersonaContactar
        String persona = obtenerPersonaContactar(puntajeTotal);
        System.out.printf("La persona a contactar es: %s \n ", persona);

    }

    static int calcularPuntajeXSensor(String sensor, String estadoSensor){
        int puntaje = 0;

        switch(estadoSensor){
            case "ok"  : puntaje = 0;
                break;

            case "error": if(sensor.equals("luz"))
                puntaje = 1;
            else if (sensor.equals("humo"))
                puntaje = 2;
            else if (sensor.equals("puerta"))
                puntaje = 3;
                break;

            case "desconectado": if(sensor.equals("luz") || sensor.equals("humo"))
                puntaje = 3;
            else if (sensor.equals("puerta"))
                puntaje = 4;
                break;
        }
        return puntaje;
    }

    static int calcularPuntajeTotal(String sensor1, String estadoSensor1,
                                    String sensor2, String estadoSensor2,
                                    String sensor3, String estadoSensor3){

        int puntaje1 = calcularPuntajeXSensor(sensor1,estadoSensor1);
        int puntaje2 = calcularPuntajeXSensor(sensor2,estadoSensor2);
        int puntaje3 = calcularPuntajeXSensor(sensor3,estadoSensor3);

        return puntaje1 + puntaje2 + puntaje3;
    }

    static String obtenerPersonaContactar(int puntajeTotal) {

        String persona = "";

        if (puntajeTotal == 0)
            persona = "Ninguno";
        else if (puntajeTotal >= 1 && puntajeTotal <= 3)
            persona = "Jefe de Operaciones";
        else if (puntajeTotal >= 4 && puntajeTotal <= 7)
            persona = "Supervisor Operaciones";
        else if (puntajeTotal >= 8)
            persona = "Gerente Operaciones";

        return persona;
    }
}

