package clase_03;

import java.util.Scanner;

public class par_o_impar {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        int numero_01,numero_02, resultado;

        System.out.println("\nIngrese el primer numero: ");
        numero_01 = sc.nextInt();

        System.out.println("\nIngrese el segundo numero: ");
        numero_02 = sc.nextInt();

        resultado = operacion_con_numeros(numero_01,numero_02);

        System.out.println("\nEl resultado de la operación es: " + resultado);
    }

    private static int operacion_con_numeros(int nu_1, int num_2) {
         int total;

        if (nu_1 % 2 == 0 && num_2 % 2 == 0) //numeros pares
            total = nu_1 + num_2;

        else if (nu_1 % 2 == 1 && num_2 % 2 == 1) //numeros impares
            total = nu_1 * num_2;

        else
            total = nu_1 - num_2;

        return total;

    }
}
