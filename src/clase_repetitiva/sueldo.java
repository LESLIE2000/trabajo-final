package clase_repetitiva;

import java.util.Scanner;

public class sueldo {

        public static void main(String[] args) {

            //01.Declaracion de Variables
            Scanner sc = new Scanner(System.in);
            int n;
            String tipoTrabajador;
            double sueldo;

            System.out.println("Ingresar número de años: ");
            n = sc.nextInt();
            sc.nextLine();
            System.out.println("Ingresar el tipo de trabajador (g: gerente, e: empleado): ");
            tipoTrabajador = sc.nextLine();
            System.out.println("Ingresar el sueldo: ");
            sueldo = sc.nextDouble();

            //Para probar método calculaSueldoNanhos
            double totalDigitos = calculaSueldoNanhos(n,tipoTrabajador,sueldo);
            System.out.printf("El sueldo es: %.2f \n", totalDigitos);

            //Para probar método calculoPorcentajeAumento
            double porcAumento = calculoPorcentajeAumento(n,tipoTrabajador,sueldo);
            System.out.printf("El porcentaje de aumento despues de N años es %.2f.\n", porcAumento);

        }

        static double calculaSueldoNanhos(int n, String tipoTrabajador, double sueldo){
            double nuevoSueldo = sueldo;
            if (tipoTrabajador.equals("g")){
                for (int i=1; i<=n; i++){
                    if (i%4==0){
                        nuevoSueldo = nuevoSueldo + nuevoSueldo * 0.18;
                    }else{
                        nuevoSueldo = nuevoSueldo + nuevoSueldo * 0.14;
                    }
                }
            }else if (tipoTrabajador.equals("e")){
                for (int i=1; i<=n; i++){
                    if (i%4==0){
                        nuevoSueldo = nuevoSueldo + nuevoSueldo * 0.12;
                    }else{
                        nuevoSueldo = nuevoSueldo + nuevoSueldo * 0.08;
                    }
                }
            }
            return nuevoSueldo;
        }

        static double calculoPorcentajeAumento(int n, String tipoTrabajador, double sueldo){
            double totalDigitos = calculaSueldoNanhos(n,tipoTrabajador,sueldo);
            double porcInc = totalDigitos*100/sueldo-100;
            return porcInc;
        }

    }
