package clase_repetitiva;

public class prueba_DO_WHILE {
    public static void main(String[] args) {
        //factorial

        int numero=5,acumulador=1,contador=1;

        do {
            acumulador*=contador;

            System.out.println("Calculo intermedio N° "+contador+ ": "+acumulador);
            contador++;

        }while(contador<=numero);
        System.out.println("\n Valor del factorial: "+ acumulador);
    }
}
