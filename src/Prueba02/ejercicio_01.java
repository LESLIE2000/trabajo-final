package Prueba02;

import java.util.Scanner;

public class ejercicio_01 {

    public static void main(String[] args) {
        double PC01,PC02,EP,EF,Promedio;

        Scanner sc = new Scanner(System.in);

        System.out.println("\nIngrese su nota de la PC01: ");
        PC01 = sc.nextDouble();

        System.out.println("Ingrese su nota de la PC02: ");
        PC02 = sc.nextDouble();

        System.out.println("Ingrese su nota de la EP: ");
        EP = sc.nextDouble();

        System.out.println("Ingrese su nota de la EF: ");
        EF = sc.nextDouble();

        Promedio = PC01 * 0.2 + PC02 * 0.2 + EP * 0.2 + EF * 0.4;

        System.out.printf("El promedio final es %.2f habiendo sacado %.2f en el examen final" , Promedio , EF);


    }
}
