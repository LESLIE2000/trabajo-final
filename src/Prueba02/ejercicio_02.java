package Prueba02;

import java.util.Scanner;

public class ejercicio_02 {

    public static void main(String[] args) {

     final double PI = 3.1416;

     double radio,area;

     Scanner sc = new Scanner(System.in);

     System.out.println("\nIngrese el radio del circulo: ");
     radio = sc.nextDouble();

     area = PI * Math.pow(radio, 2);

     System.out.printf("El area del círculo con radio %.2f es: %.2f" , radio, area);

    }
}
